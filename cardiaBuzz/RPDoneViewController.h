//
//  RPDoneViewController.h
//  cardiaBuzz
//
//  Created by Reza Pourabolghasem on 2/2/13.
//  Copyright (c) 2013 Georgia Tech Research Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPStartViewController.h"
#import "RPFinishViewController.h"

@interface RPDoneViewController : UIViewController <MyMasterRoot, UpdateDoneViewProtocol>

- (IBAction)done;

@property (weak,nonatomic) RPStartViewController *myMasterRoot;
@property (weak, nonatomic) IBOutlet UILabel *outputResult;
@property (weak, nonatomic) IBOutlet UIView *labelBorder;

@end
