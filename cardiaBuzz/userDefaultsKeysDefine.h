//
//  userDefaultsKeysDefine.h
//  cardiaBuzz
//
//  Created by Reza Pourabolghasem on 2/4/13.
//  Copyright (c) 2013 Georgia Tech Research Corporation. All rights reserved.
//

#ifndef cardiaBuzz_userDefaultsKeysDefine_h
#define cardiaBuzz_userDefaultsKeysDefine_h

#ifndef RECORDS_KEY
#define RECORDS_KEY @"cardiaBuzz.MeasurementData"
#endif

#ifndef PROFILE_KEY
#define PROFILE_KEY @"cardiaBuzz.ProfileData"
#endif


#endif
