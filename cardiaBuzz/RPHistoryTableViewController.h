//
//  RPHistoryTableViewController.h
//  cardiaBuzz
//
//  Created by Reza Pourabolghasem on 2/3/13.
//  Copyright (c) 2013 Georgia Tech Research Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>


@interface RPHistoryTableViewController : UITableViewController <MFMailComposeViewControllerDelegate>
- (IBAction)emailHistoryPressed:(id)sender;

@end
