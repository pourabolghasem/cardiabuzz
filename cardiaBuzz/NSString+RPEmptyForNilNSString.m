//
//  NSString+RPEmptyForNilNSString.m
//  cardiaBuzz
//
//  Created by Reza Pourabolghasem on 2/5/13.
//  Copyright (c) 2013 Georgia Tech Research Corporation. All rights reserved.
//

#import "NSString+RPEmptyForNilNSString.h"

@implementation NSString (RPEmptyForNilNSString)
+(NSString *)emptyForNil:(NSString *)string {
    if (!string) return @"";
    return string;
}
@end
