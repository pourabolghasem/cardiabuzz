//
//  RPMeasurementData.m
//  cardiaBuzz
//
//  Created by Reza Pourabolghasem on 2/3/13.
//  Copyright (c) 2013 Georgia Tech Research Corporation. All rights reserved.
//

#import "RPMeasurementData.h"

@interface RPMeasurementData ()

@property (strong, nonatomic) NSDate *dateTime;
@property (strong, nonatomic) NSString *deviceId;
@property (strong, nonatomic) NSNumber *troponin;
@property (strong, nonatomic) NSNumber *bnp;

@end

@implementation RPMeasurementData

@synthesize dateTime = _dateTime;
@synthesize deviceId = _deviceId;
@synthesize troponin = _troponin;
@synthesize bnp = _bnp;

- (RPMeasurementData *)initWithRandom {
    self = [super init];
    self.dateTime = [NSDate date];
    self.deviceId = [[@"TD" stringByAppendingFormat:@"%d",arc4random_uniform(10)] stringByAppendingString:@"0064RP513"];
    self.troponin = [NSNumber numberWithInt:(arc4random_uniform(10) + 17)];
    self.bnp = [NSNumber numberWithInt:(arc4random_uniform(750)+200)];
    return self;
}

- (NSString *)formattedDate {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"EEE MMM d, YYYY hh:mm a"];
    return [dateFormat stringFromDate:self.dateTime];
}

- (NSString *)description {
    NSString *desc = [NSString stringWithFormat:@"%@\nDevice Id: %@\nTroponin-I: %@ pg/mL\nBNP: %@ pg/mL", [self formattedDate], self.deviceId, @"--", @"--"];
    return desc;
}

@end
