//
//  RPAdvancedProfileTableViewController.h
//  cardiaBuzz
//
//  Created by Reza Pourabolghasem on 2/5/13.
//  Copyright (c) 2013 Georgia Tech Research Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPBirthDateViewController.h"

@interface RPAdvancedProfileTableViewController : UITableViewController <UITextFieldDelegate, RPBirthDateDelegate>

- (IBAction)donePressed:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *personNameField;
@property (weak, nonatomic) IBOutlet UITextField *personBirthDateField;
@property (weak, nonatomic) IBOutlet UITextField *physNameField;
@property (weak, nonatomic) IBOutlet UITextField *physPhoneField;
@property (weak, nonatomic) IBOutlet UITextField *physEmailField;
@property (weak, nonatomic) IBOutlet UITextField *familyNameField;
@property (weak, nonatomic) IBOutlet UITextField *familyPhoneField;
@property (weak, nonatomic) IBOutlet UITextField *familyEmailField;

@end
