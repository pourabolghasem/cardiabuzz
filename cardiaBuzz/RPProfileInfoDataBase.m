//
//  RPProfileInfoDataBase.m
//  cardiaBuzz
//
//  Created by Reza Pourabolghasem on 2/6/13.
//  Copyright (c) 2013 Georgia Tech Research Corporation. All rights reserved.
//

#import "RPProfileInfoDataBase.h"
#import <UIKit/UIKit.h>
#import "NSString+RPEmptyForNilNSString.h"

@implementation RPProfileInfoDataBase

@synthesize keys = _keys;
@synthesize profileInfo = _profileInfo;


- (RPProfileInfoDataBase *)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    [self setKeys];
    self.profileInfo = dict;
    return self;
}

- (NSDictionary *)profileInfo {
    if (!_profileInfo) {
        NSMutableArray *emptyStrings = [NSMutableArray array];
        for (NSString *key in self.keys) [emptyStrings addObject:@""];
        _profileInfo = [NSDictionary dictionaryWithObjects:emptyStrings forKeys:self.keys];
    }
    return _profileInfo;
}

- (void)setKeys {
    _keys = @[
              @"personName",
              @"personBirthDate",
              @"physName",
              @"physPhone",
              @"physEmail",
              @"familyName",
              @"familyPhone",
              @"familyEmail"
              ];
}

- (NSString *)objectForKeyedSubscript:(id)key {
    return self.profileInfo[key];
}

- (NSString *)physEmail {
    return [NSString emptyForNil:self.profileInfo[@"physEmail"]];
}

- (NSString *)personaName {
    return [NSString emptyForNil:self.profileInfo[@"personName"]];
}


@end
