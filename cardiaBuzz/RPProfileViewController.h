//
//  RPProfileViewController.h
//  cardiaBuzz
//
//  Created by Reza Pourabolghasem on 2/3/13.
//  Copyright (c) 2013 Georgia Tech Research Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPProfileViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *birthDateField;
- (IBAction)donePressed:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *physNameField;
@property (weak, nonatomic) IBOutlet UITextField *physEmailField;

@end
