//
//  RPFinishViewController.m
//  cardiaBuzz
//
//  Created by Reza Pourabolghasem on 2/2/13.
//  Copyright (c) 2013 Georgia Tech Research Corporation. All rights reserved.
//

#import "RPFinishViewController.h"
#import <stdlib.h>
#import "userDefaultsKeysDefine.h"


@interface RPFinishViewController ()
- (void)callSegueFinish:(id)sender;
@end

@implementation RPFinishViewController

@synthesize myMasterRoot = _myMasterRoot;
@synthesize measurementRecord = _measurementRecord;

- (RPMeasurementData *)measurementRecord {
    if (!_measurementRecord) {
        _measurementRecord = [[RPMeasurementData alloc] initWithRandom];
    }
    return _measurementRecord;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)callSegueFinish:(id)sender {
    [self performSegueWithIdentifier:@"Finish" sender:sender];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self performSelector:@selector(callSegueFinish:) withObject:self afterDelay:5];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"Finish"]) {
        [self saveMeasurementData];
         id <MyMasterRoot,UpdateDoneViewProtocol> dest =
        segue.destinationViewController;
        dest.myMasterRoot = self.myMasterRoot;
        [dest setDoneViewLabel:[self.measurementRecord description]];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)saveMeasurementData {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray *records = [[defaults objectForKey:RECORDS_KEY] mutableCopy];
    if (!records) records = [NSMutableArray array];
    
    [records addObject:[self.measurementRecord description]];
    [defaults setObject:records forKey:RECORDS_KEY];
    [defaults synchronize];
}
@end







