//
//  RPProfileInfoDataBase.h
//  cardiaBuzz
//
//  Created by Reza Pourabolghasem on 2/6/13.
//  Copyright (c) 2013 Georgia Tech Research Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RPProfileInfoDataBase : NSObject

@property (strong, nonatomic) NSDictionary *profileInfo;
@property (strong, nonatomic, readonly) NSArray* keys;
@property (weak, nonatomic, readonly) NSString *physEmail;
@property (weak, nonatomic, readonly) NSString *personaName;

- (RPProfileInfoDataBase *)initWithDictionary:(NSDictionary *)dict;
- (NSString *)objectForKeyedSubscript:(id)key;

@end
