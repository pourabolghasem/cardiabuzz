//
//  RPAdvancedProfileTableViewController.m
//  cardiaBuzz
//
//  Created by Reza Pourabolghasem on 2/5/13.
//  Copyright (c) 2013 Georgia Tech Research Corporation. All rights reserved.
//

#import "RPAdvancedProfileTableViewController.h"
#import "userDefaultsKeysDefine.h"
#import "NSString+RPEmptyForNilNSString.h"
#import "RPProfileInfoDataBase.h"

@interface RPAdvancedProfileTableViewController ()
@property (strong, nonatomic) RPProfileInfoDataBase *profile;

- (void)setFieldData;
- (void)getFieldData;
- (void)updateProfileDefaults;
@end

@implementation RPAdvancedProfileTableViewController

@synthesize profile = _profile;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    return self;
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:NO];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *profile = [defaults objectForKey:PROFILE_KEY];
    self.profile = [[RPProfileInfoDataBase alloc] initWithDictionary:profile];
    [self setFieldData];
}

- (void)setFieldData {
    self.personNameField.text = [NSString emptyForNil:self.profile[self.profile.keys[0]]];
    self.personBirthDateField.text = [NSString emptyForNil:self.profile[self.profile.keys[1]]];
    self.physNameField.text = [NSString emptyForNil:self.profile[self.profile.keys[2]]];
    self.physPhoneField.text = [NSString emptyForNil:self.profile[self.profile.keys[3]]];
    self.physEmailField.text = [NSString emptyForNil:self.profile[self.profile.keys[4]]];
    self.familyNameField.text = [NSString emptyForNil:self.profile[self.profile.keys[5]]];
    self.familyPhoneField.text = [NSString emptyForNil:self.profile[self.profile.keys[6]]];
    self.familyEmailField.text = [NSString emptyForNil:self.profile[self.profile.keys[7]]];
}

- (void)getFieldData {
    self.profile.profileInfo = @{
                         self.profile.keys[0] : self.personNameField.text,
                         self.profile.keys[1] : self.personBirthDateField.text,
                         self.profile.keys[2] : self.physNameField.text,
                         self.profile.keys[3] : self.physPhoneField.text,
                         self.profile.keys[4] : self.physEmailField.text,
                         self.profile.keys[5] : self.familyNameField.text,
                         self.profile.keys[6] : self.familyPhoneField.text,
                         self.profile.keys[7] : self.familyEmailField.text
                         };
}

- (void)updateProfileDefaults {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:self.profile.profileInfo forKey:PROFILE_KEY];
    [defaults synchronize];
}

- (IBAction)donePressed:(id)sender {
    [self getFieldData];
    [self updateProfileDefaults];
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    [self getFieldData];
    [self updateProfileDefaults];
    return NO;
}

- (void)updateBirthDate:(NSString *)birthDate {
    self.personBirthDateField.text = birthDate;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"Birth Date"]) {
        RPBirthDateViewController *dest = segue.destinationViewController;
        dest.delegate = self;
        dest.birthDate = [NSString emptyForNil:self.profile[self.profile.keys[1]]];
    }
}

- (void)viewDidUnload {
    [self setPersonNameField:nil];
    [self setPersonBirthDateField:nil];
    [self setPhysNameField:nil];
    [self setPhysPhoneField:nil];
    [self setPhysEmailField:nil];
    [self setFamilyNameField:nil];
    [self setFamilyPhoneField:nil];
    [self setFamilyEmailField:nil];
    [super viewDidUnload];
}
@end
