//
//  RPProfileViewController.m
//  cardiaBuzz
//
//  Created by Reza Pourabolghasem on 2/3/13.
//  Copyright (c) 2013 Georgia Tech Research Corporation. All rights reserved.
//

#import "RPProfileViewController.h"
#import "userDefaultsKeysDefine.h"

@interface RPProfileViewController () {
    NSArray *keyNames;
}

@property (strong,nonatomic) NSDictionary *profileInfo;
-(void)setKeyNames;
-(void)setFieldData;
-(void)getFieldData;
-(void)updateProfileDefaults;
-(NSString *)emptyForNil:(NSString *)string;

@end

@implementation RPProfileViewController

@synthesize profileInfo = _profileInfo;

- (NSDictionary *)profileInfo {
    if (!_profileInfo) {
        _profileInfo = [NSDictionary dictionary];
    }
    return _profileInfo;
}

-(NSString *)emptyForNil:(NSString *)string {
    if (!string) return @"";
    return string;
}

-(void)setKeyNames {
    keyNames = @[
                 @"personName",
                 @"personBirthDate",
                 @"physName",
                 @"physEmail"
                 ];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setKeyNames];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    id profile = [defaults objectForKey:PROFILE_KEY];
    if (![profile isKindOfClass:[NSDictionary class]] ) profile = nil;
    self.profileInfo = profile;
    [self setFieldData];
}

-(void)setFieldData {
    self.nameField.text = [self emptyForNil:self.profileInfo[keyNames[0]]];
    self.birthDateField.text = [self emptyForNil:self.profileInfo[keyNames[1]]];
    self.physNameField.text = [self emptyForNil:self.profileInfo[keyNames[2]]];
    self.physEmailField.text = [self emptyForNil:self.profileInfo[keyNames[3]]];
}

-(void)getFieldData {
    self.profileInfo = @{
                         keyNames[0] : self.nameField.text,
                         keyNames[1] : self.birthDateField.text,
                         keyNames[2] : self.physNameField.text,
                         keyNames[2] : self.physEmailField.text
                         };
}

-(void)updateProfileDefaults {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:self.profileInfo forKey:PROFILE_KEY];
    [defaults synchronize];
}

-(IBAction)donePressed:(id)sender {
    [self getFieldData];
    [self updateProfileDefaults];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    keyNames = nil;
    [self setNameField:nil];
    [self setBirthDateField:nil];
    [self setPhysNameField:nil];
    [self setPhysEmailField:nil];
    [super viewDidUnload];
}
@end
