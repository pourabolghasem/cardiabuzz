//
//  RPBirthDateViewController.m
//  cardiaBuzz
//
//  Created by Reza Pourabolghasem on 2/6/13.
//  Copyright (c) 2013 Georgia Tech Research Corporation. All rights reserved.
//

#import "RPBirthDateViewController.h"

@interface RPBirthDateViewController ()

@end

@implementation RPBirthDateViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MMM d, yyyy"];
    self.birthDateField.text = self.birthDate;
    
    @try {
        self.datePicker.date = [dateFormat dateFromString:self.birthDate];
    }
    @catch (NSException *exception) {
        self.datePicker.date = [dateFormat dateFromString:@"Jan 1, 1950"];
    }
    
    self.datePicker.maximumDate = [NSDate date];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setBirthDateField:nil];
    [self setDatePicker:nil];
    [super viewDidUnload];
}

- (IBAction)dateChanged {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MMM d, yyyy"];
    self.birthDateField.text = [dateFormat stringFromDate:self.datePicker.date];
    [self.delegate updateBirthDate:self.birthDateField.text];
}


@end





