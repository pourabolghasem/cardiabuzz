//
//  NSString+RPEmptyForNilNSString.h
//  cardiaBuzz
//
//  Created by Reza Pourabolghasem on 2/5/13.
//  Copyright (c) 2013 Georgia Tech Research Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (RPEmptyForNilNSString)
+(NSString *)emptyForNil:(NSString *)string;
@end
