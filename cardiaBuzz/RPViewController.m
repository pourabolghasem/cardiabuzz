//
//  RPViewController.m
//  cardiaBuzz
//
//  Created by Reza Pourabolghasem on 2/2/13.
//  Copyright (c) 2013 Georgia Tech Research Corporation. All rights reserved.
//

#import "RPViewController.h"

@interface RPViewController ()
@property (strong,nonatomic) NSArray *options;
@property (strong,nonatomic) NSArray *imageFiles;

@end

@implementation RPViewController
@synthesize options = _options;
@synthesize imageFiles = _imageFiles;
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.options = @[@"New Test", @"History", @"Profile", @"Info"];
    self.imageFiles = @[@"test.png", @"historySized.png", @"profile.png", @"info.png"];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.options count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"OptionCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.text = [self.options objectAtIndex:indexPath.row];
    cell.imageView.image = [UIImage imageNamed:[self.imageFiles objectAtIndex:indexPath.row]];
   
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0)
        [self performSegueWithIdentifier:@"New Test" sender:self];
    else if (indexPath.row == 1)
        [self performSegueWithIdentifier:@"History" sender:self];
    else if (indexPath.row == 2)
        [self performSegueWithIdentifier:@"Profile" sender:self];
    else if (indexPath.row == 3)
        [self performSegueWithIdentifier:@"Info" sender:self];
}

- (void)viewDidUnload {
    [self setTableView:nil];
    [super viewDidUnload];
}
@end
