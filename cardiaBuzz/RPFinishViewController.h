//
//  RPFinishViewController.h
//  cardiaBuzz
//
//  Created by Reza Pourabolghasem on 2/2/13.
//  Copyright (c) 2013 Georgia Tech Research Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPStartViewController.h"
#import "RPMeasurementData.h"

@protocol UpdateDoneViewProtocol

- (void)setDoneViewLabel:(NSString *)description;

@end

@interface RPFinishViewController : UIViewController <MyMasterRoot>

@property (weak,nonatomic) RPStartViewController *myMasterRoot;
@property (strong,nonatomic) RPMeasurementData *measurementRecord;

@end
