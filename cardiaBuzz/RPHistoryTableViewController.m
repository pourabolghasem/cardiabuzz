//
//  RPHistoryTableViewController.m
//  cardiaBuzz
//
//  Created by Reza Pourabolghasem on 2/3/13.
//  Copyright (c) 2013 Georgia Tech Research Corporation. All rights reserved.
//

#import "RPHistoryTableViewController.h"
#include <QuartzCore/QuartzCore.h>
#import "userDefaultsKeysDefine.h"
#import "RPProfileInfoDataBase.h"

@interface RPHistoryTableViewController ()

@property (strong, nonatomic, readonly) NSArray *historyArray;
@property (strong, nonatomic, readonly) RPProfileInfoDataBase *profile;

- (NSString *)historyString;

@end

@implementation RPHistoryTableViewController

@synthesize historyArray = _historyArray;
@synthesize profile = _profile;

- (RPProfileInfoDataBase *)profile {
    if (!_profile) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        _profile = [[RPProfileInfoDataBase alloc] initWithDictionary:[defaults objectForKey:PROFILE_KEY]];
    }
    return _profile;
}

- (NSArray *)historyArray {
    if (!_historyArray) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        _historyArray = [defaults objectForKey:RECORDS_KEY];
        if (!_historyArray) _historyArray = [NSArray array];
    }
    return _historyArray;
}


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.historyArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"History Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    cell.textLabel.numberOfLines = 4;
    cell.textLabel.text = [self.historyArray objectAtIndex:self.historyArray.count - indexPath.row - 1];
    
    return cell;
}

- (NSString *)historyString {
    NSMutableString *strTotal = [NSMutableString string];
    for (NSString *str in self.historyArray)
        [strTotal appendFormat:@"%@\n\n", str];
    return strTotal;
}

- (IBAction)emailHistoryPressed:(id)sender {
    if([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        [controller setToRecipients:@[self.profile.physEmail]];
        [controller setSubject:[@"Test Results: " stringByAppendingString:self.profile.personaName]];
        [controller setMessageBody:[self historyString] isHTML:NO];
        if (controller) [self presentModalViewController:controller animated:YES];
    } else {
        UIAlertView*emailAlert=[[UIAlertView alloc] initWithTitle:@"No Email Account Found." message:@"Please set an email account." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [emailAlert show];
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    if (result == MFMailComposeResultSent) {
        NSLog(@"It's away!");
    }
    [self dismissModalViewControllerAnimated:YES];
}
@end
