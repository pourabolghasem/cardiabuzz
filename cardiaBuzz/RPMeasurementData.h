//
//  RPMeasurementData.h
//  cardiaBuzz
//
//  Created by Reza Pourabolghasem on 2/3/13.
//  Copyright (c) 2013 Georgia Tech Research Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RPMeasurementData : NSObject
- (RPMeasurementData *)initWithRandom;

@end
