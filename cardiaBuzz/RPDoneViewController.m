//
//  RPDoneViewController.m
//  cardiaBuzz
//
//  Created by Reza Pourabolghasem on 2/2/13.
//  Copyright (c) 2013 Georgia Tech Research Corporation. All rights reserved.
//

#import "RPDoneViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface RPDoneViewController ()

@property (strong, nonatomic) NSString *lableText;

@end

@implementation RPDoneViewController

@synthesize myMasterRoot = _myMasterRoot;
@synthesize lableText = _lableText;

- (IBAction)done {
    [self.myMasterRoot releaseEverything];
}

- (void)setDoneViewLabel:(NSString *)description {
    self.lableText = description;
//    self.outputResult.text = description;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.outputResult.text = self.lableText;
    self.labelBorder.layer.borderWidth = (CGFloat)1.0;
    self.labelBorder.layer.cornerRadius = (CGFloat)8.0;
    [self performSelector:@selector(done) withObject:self afterDelay:10];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setOutputResult:nil];
    [self setLabelBorder:nil];
    [super viewDidUnload];
}
@end
