//
//  main.m
//  cardiaBuzz
//
//  Created by Reza Pourabolghasem on 2/2/13.
//  Copyright (c) 2013 Georgia Tech Research Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RPAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RPAppDelegate class]));
    }
}
