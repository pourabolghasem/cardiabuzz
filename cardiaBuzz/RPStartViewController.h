//
//  RPStartViewController.h
//  cardiaBuzz
//
//  Created by Reza Pourabolghasem on 2/2/13.
//  Copyright (c) 2013 Georgia Tech Research Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RPStartViewController;

@protocol ReleaseEverything

- (void)releaseEverything;

@end

@protocol MyMasterRoot

- (RPStartViewController *)myMasterRoot;
- (void)setMyMasterRoot:(RPStartViewController *)masterRoot;

@end

@interface RPStartViewController : UIViewController <ReleaseEverything>

@end
