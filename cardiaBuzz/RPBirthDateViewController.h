//
//  RPBirthDateViewController.h
//  cardiaBuzz
//
//  Created by Reza Pourabolghasem on 2/6/13.
//  Copyright (c) 2013 Georgia Tech Research Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RPBirthDateDelegate

- (void)updateBirthDate:(NSString *)birthDate;

@end

@interface RPBirthDateViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *birthDateField;
@property (strong, nonatomic) NSString *birthDate;
@property (weak, nonatomic) id <RPBirthDateDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
- (IBAction)dateChanged;

@end
