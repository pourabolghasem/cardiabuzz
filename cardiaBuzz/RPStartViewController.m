//
//  RPStartViewController.m
//  cardiaBuzz
//
//  Created by Reza Pourabolghasem on 2/2/13.
//  Copyright (c) 2013 Georgia Tech Research Corporation. All rights reserved.
//

#import "RPStartViewController.h"

@interface RPStartViewController ()

@end

@implementation RPStartViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"Start"]) {
        id <MyMasterRoot> dest =
        segue.destinationViewController;
        dest.myMasterRoot = self;
    }
}

- (void)releaseEverything {
    [self dismissViewControllerAnimated:YES completion:NULL];
    [self.navigationController popToRootViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
