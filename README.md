# CardiaBuzz Health Data Aggregator

CardiaBuzz is a minimalistic app that is designed to connect to a measurement device and gather the health data and store them on the iPhone. The user can view the history of the measurements and can update his/her physician via built-in email sharing option.

In its current iteration, user interface, data storage, and email sharing is implemented.